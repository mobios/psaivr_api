package com.mobios.bank.controller;



import org.springframework.beans.factory.annotation.Autowired;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


import com.mobios.bank.account.Account;
import com.mobios.bank.model.Member_info;
import com.mobios.bank.model.RequestAccountDetails;
import com.mobios.bank.model.RequestMemberID;
import com.mobios.bank.model.ResponseMemberID;
import com.mobios.bank.service.MemberInfoService;
import com.mobios.bank.validation.AccountNotFoundException;
import com.mobios.bank.validation.ErrorResponse;
import com.mobios.bank.validation.PINNotValidException;
import com.mobios.bank.validation.PSAExceptionCodes;


@RestController
@CrossOrigin(origins = "*")
public class MemberController {
	
	@Autowired
	MemberInfoService memberinfoservice;
	
	
	@RequestMapping(value="/getMemberID/", method=RequestMethod.POST ,headers = "Accept=application/json")
	public ResponseMemberID getMemberID(@RequestBody RequestMemberID request) throws  Exception{
		 
		Member_info memberinfo = new Member_info();
		ResponseMemberID _response =new ResponseMemberID();

		   memberinfo = memberinfoservice.getMemberID(request.getPin(),request.getMsisdn());
          
		   if(memberinfo == null) {
        	
			   throw new PINNotValidException();
        	
           }
		   _response.setMember_id(memberinfo.getMember_id());
	
		return _response;
			
	}
	
	@ExceptionHandler(PINNotValidException.class)
	public ResponseEntity<ErrorResponse> exceptionHandler(Exception ex) {
		ErrorResponse error = new ErrorResponse();
		error.setErrorCode(PSAExceptionCodes.PIN_VALIDATION_FAIL);
		error.setMessage(ex.getMessage());
		return new ResponseEntity<ErrorResponse>(error, HttpStatus.OK);
	}
	
	@RequestMapping(value="/getAccountDetails/", method=RequestMethod.POST , headers="Accept=application/json")
	public Account getAccountDetails(@RequestBody RequestAccountDetails request)  throws Exception {
		
		Account account = new Account();
		Member_info memberinfo = new Member_info();
		ResponseMemberID response = new ResponseMemberID();
	
		memberinfo = memberinfoservice.getMemberID(request.getPin(),request.getMsisdn());
		 if(memberinfo == null) {
	        	
			   throw new PINNotValidException();
      	
         }
		response.setMember_id(memberinfo.getMember_id());
		
		      account = memberinfoservice.getAccountDetails(request.getPin(),request.getMsisdn(),response.getMember_id(),request.getAccountType());
		   
		  if( account.getAccountType() == 0 ) {
		    	
		    	throw new AccountNotFoundException();
		    	
		    } 
		      	
		return account;
	}
	
	@ExceptionHandler(AccountNotFoundException.class)
	public ResponseEntity<ErrorResponse> exceptionHandler2(Exception ex) {
		ErrorResponse error = new ErrorResponse();
		error.setErrorCode(PSAExceptionCodes.ACCOUNT_TYPE_NOT_FOUND);
		error.setMessage(ex.getMessage());
		return new ResponseEntity<ErrorResponse>(error, HttpStatus.OK);
	}




}
