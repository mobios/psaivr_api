package com.mobios.bank.model;

public class RequestMemberID {
	
	private String pin;
	private String msisdn;
	

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}
	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn ) {
		this.msisdn = msisdn;
	}
	@Override
	public String toString() {
		return "RequestMemberID [pin=" + pin + ",  msisdn="+ msisdn + "]";
	}

}
