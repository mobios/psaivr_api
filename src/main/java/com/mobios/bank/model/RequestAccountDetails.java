package com.mobios.bank.model;

public class RequestAccountDetails {
	
	private String pin;
	private String msisdn;
	private String member_id;
	private int accountType;
	
	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}
	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn ) {
		this.msisdn = msisdn;
	}
	public String getMember_id() {
		return member_id;
	}

	public void setMember_id(String member_id) {
		this.member_id = member_id;
	}
	public int getAccountType() {
		return accountType;
	}

	public void setAccountType(int accountType) {
		this.accountType = accountType;
	}

	@Override
	public String toString() {
		return "RequestMemberID [pin=" + pin + ",  msisdn="+ msisdn + ",member_id="+member_id+",accountType="+accountType+"]";
	}

}
