package com.mobios.bank.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="member_info")
public class Member_info {
	
        @Id
        @GeneratedValue
		@Column(name="id")
		private int id;
		@Column(name="name")
		private String name;
		@Column(name="member_id") 
		private String member_id;
		@Column(name="pin")
		private String pin;
		@Column(name="nic")
		private String nic;
		@Column(name="division")
		private String division;
		@Column(name="rank")
		private String rank;
		@Column(name="station")
		private String station;
		@Column(name="created_date")
		private String created_date;
		@Column (name ="updated_date")
		private String updated_date;
		@Column(name="created_user")
		private String created_user;
		@Column(name="msisdn")
		private String msisdn;
		@Column(name="info1")
		private String info1;
		@Column(name="info2")
		private String info2;
		@Column(name="info3")
		private String info3;
		@Column(name="delete_status")
		private String delete_status;
		
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getMember_id() {
			return member_id;
		}
		public void setMember_id(String member_id) {
			this.member_id = member_id;
		}
		public String getPin() {
			return pin;
		}
		public void setPin(String pin) {
			this.pin = pin;
		}
		public String getNic() {
			return nic;
		}
		public void setNic(String nic) {
			this.nic = nic;
		}
		public String getDivision() {
			return division;
		}
		public void setDivision(String division) {
			this.division = division;
		}
		public String getRank() {
			return rank;
		}
		public void setRank(String rank) {
			this.rank = rank;
		}
		public String getStation() {
			return station;
		}
		public void setStation(String station) {
			this.station = station;
		}
		
		public String getCreated_date() {
			return created_date;
		}
		public void setCreated_date(String created_date) {
			this.created_date = created_date;
		}
		public String getUpdated_date() {
			return updated_date;
		}
		public void setUpdated_date(String updated_date) {
			this.updated_date = updated_date;
		}
		public String getCreated_user() {
			return created_user;
		}
		public void setCreated_user(String created_user) {
			this.created_user = created_user;
		}
		public String getMsisdn() {
			return msisdn;
		}
		public void setMsisdn(String msisdn) {
			this.msisdn = msisdn;
		}
		public String getInfo1() {
			return info1;
		}
		public void setInfo1(String info1) {
			this.info1 = info1;
		}
		public String getInfo2() {
			return info2;
		}
		public void setInfo2(String info2) {
			this.info2 = info2;
		}
		public String getInfo3() {
			return info3;
		}
		public void setInfo3(String info3) {
			this.info3 = info3;
		}
		public String getDelete_status() {
			return delete_status;
		}
		public void setDelete_status(String delete_status) {
			this.delete_status = delete_status;
		}
			

}
