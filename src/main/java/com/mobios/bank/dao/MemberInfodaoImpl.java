package com.mobios.bank.dao;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mobios.bank.model.Member_info;
import com.mobios.bank.validation.PINNotValidException;


@Repository
public class MemberInfodaoImpl implements MemberInfoDao{
	
	@Autowired
	private SessionFactory sessionFactory;

	protected Session getSession() {
		return sessionFactory.getCurrentSession();
	}
	

	public Member_info getMemberID(String pin, String msisdn) throws PINNotValidException {
		
		Member_info memberinfo = new Member_info();
		
		Query query = getSession().createQuery("FROM Member_info WHERE pin= '" + pin + "' AND msisdn= '" + msisdn + "'");
		
		if(query.list().size()!=0) {
			memberinfo = (Member_info) query.list().get(0);
		}
		else {
			throw new PINNotValidException();
		}
		return memberinfo;
	}		
		
 
	

}
  