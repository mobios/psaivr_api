package com.mobios.bank.dao;

import com.mobios.bank.model.Member_info;
import com.mobios.bank.validation.PINNotValidException;


public interface MemberInfoDao {
	
	public Member_info getMemberID(String pin, String msisdn) throws PINNotValidException;

}
