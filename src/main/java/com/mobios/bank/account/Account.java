package com.mobios.bank.account;

import java.math.BigDecimal;
import java.util.UUID;

public class Account {

	public static final int CS = 10;
	public static final int CL = 20;
	public static final int SL = 30;
	public static final int ML = 40;//-931
	public static final float ERROR_CODE_1 = -911;
	public static final float ERROR_CODE_2 = -931;
	public static final float ERROR_BALANCE = (float) 0.00;
	


	private String referenceNo = UUID.randomUUID().toString();
	private int accountType;
	private BigDecimal payableAmount;
	private BigDecimal installmentAmount;
	private BigDecimal intallmentInterest;
	private BigDecimal bonus;
	private BigDecimal additionalAmount;
	

	public BigDecimal getBonus() {
		return bonus;
	}

	public void setBonus(BigDecimal bonus) {
		this.bonus = bonus.setScale(2,BigDecimal.ROUND_HALF_EVEN);
	}

	public int getAccountType() {
		return accountType;
	}

	public Account(BigDecimal payAmount, BigDecimal insAmount, BigDecimal insInterest,BigDecimal bonus,BigDecimal additionalAmount) {
		this.payableAmount = payAmount;
		this.installmentAmount = insAmount;
		this.intallmentInterest = insInterest;
		this.bonus =bonus;
		this.bonus =additionalAmount;
	}

		
	public String getReferenceNo() {
		return referenceNo;
	}

	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}
	
	public Account() {
	}

		public void setAccountType(int accountType) {
		this.accountType = accountType;
	}
		
	public BigDecimal getPayableAmount() {
			return payableAmount;
		}

		public void setPayableAmount(BigDecimal payableAmount) {
			
			if(payableAmount.equals(new BigDecimal(ERROR_CODE_1)) || payableAmount.equals(new BigDecimal(ERROR_CODE_2))){
				this.payableAmount=new BigDecimal(ERROR_BALANCE).setScale(2,BigDecimal.ROUND_HALF_EVEN);
			}else{
				this.payableAmount = payableAmount.setScale(2,BigDecimal.ROUND_HALF_EVEN);
			}
			
		}

		public BigDecimal getInstallmentAmount() {
			return installmentAmount;
		}

		public void setInstallmentAmount(BigDecimal installmentAmount) {
			if(installmentAmount.equals(new BigDecimal(ERROR_CODE_1)) || installmentAmount.equals(new BigDecimal(ERROR_CODE_2))){
				this.installmentAmount=new BigDecimal(ERROR_BALANCE).setScale(2,BigDecimal.ROUND_HALF_EVEN);
			}else{
				this.installmentAmount = installmentAmount.setScale(2,BigDecimal.ROUND_HALF_EVEN);
			}
		}

		public BigDecimal getIntallmentInterest() {
			return intallmentInterest;
		}

		public void setIntallmentInterest(BigDecimal intallmentInterest) {
			if(intallmentInterest.equals(new BigDecimal(ERROR_CODE_1)) || intallmentInterest.equals(new BigDecimal(ERROR_CODE_2))){
				this.intallmentInterest=new BigDecimal(ERROR_BALANCE).setScale(2,BigDecimal.ROUND_HALF_EVEN);
			}else{
				this.intallmentInterest = intallmentInterest.setScale(2,BigDecimal.ROUND_HALF_EVEN);
			}
		}
		public BigDecimal getAdditionalAmount() {
			return additionalAmount;
		}

		public void setAdditionalAmount(BigDecimal additionalAmount) {
			if(additionalAmount.equals(new BigDecimal(ERROR_CODE_1)) || additionalAmount.equals(new BigDecimal(ERROR_CODE_2))){
				this.additionalAmount=new BigDecimal(ERROR_BALANCE).setScale(2,BigDecimal.ROUND_HALF_EVEN);
			}else{
				this.additionalAmount = additionalAmount.setScale(2,BigDecimal.ROUND_HALF_EVEN);
			}
		}



}
