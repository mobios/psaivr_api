package com.mobios.bank.account;

import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;


public class RegisterdMember implements java.sql.SQLData{
	
	 int REF_NO;
	int SML_PAY_AMT;
	int INSTL_AMT;
	int INSTL_INT;
	private String sql_type;
	
	
	public int getREF_NO() {
		return REF_NO;
	}
	public void setREF_NO(int rEF_NO) {
		REF_NO = rEF_NO;
	}
	public int getSML_PAY_AMT() {
		return SML_PAY_AMT;
	}
	public void setSML_PAY_AMT(int sML_PAY_AMT) {
		SML_PAY_AMT = sML_PAY_AMT;
	}
	public int getINSTL_AMT() {
		return INSTL_AMT;
	}
	public void setINSTL_AMT(int iNSTL_AMT) {
		INSTL_AMT = iNSTL_AMT;
	}
	public int getINSTL_INT() {
		return INSTL_INT;
	}
	public void setINSTL_INT(int iNSTL_INT) {
		INSTL_INT = iNSTL_INT;
	}
	
	public String getSQLTypeName() throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	public void readSQL(SQLInput stream, String typeName) throws SQLException {

		sql_type=typeName;
		REF_NO=stream.readInt();
		SML_PAY_AMT=stream.readInt();
		INSTL_AMT=stream.readInt();
		INSTL_INT =stream.readInt();
	}
	
	public void writeSQL(SQLOutput stream) throws SQLException {
		stream.writeInt(REF_NO);;
		stream.writeInt(SML_PAY_AMT);
		stream.writeInt(INSTL_AMT);
		stream.writeInt(INSTL_INT);
	}
	 	

}
