package com.mobios.bank.config;

import java.util.Properties;

import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@ComponentScan({"com.mobios.bank.config"})
public class HibernateConfig {
	
	@Primary
	@Bean
	public LocalSessionFactoryBean sessionFactoryBean() {
		LocalSessionFactoryBean sessionFactoryBean = new LocalSessionFactoryBean();
		sessionFactoryBean.setDataSource(dataSource());
		sessionFactoryBean.setPackagesToScan(new String[] {"com.mobios.bank.model"});
		sessionFactoryBean.setHibernateProperties(hibernateProperties());
	 
		return sessionFactoryBean;
		
	} 
	
	@Primary
	@Bean
	public DataSource dataSource() {
		DriverManagerDataSource ds = new DriverManagerDataSource();
		ds.setDriverClassName("com.mysql.jdbc.Driver");
		ds.setUrl("jdbc:mysql://localhost:3306/psa?zeroDateTimeBehavior=convertToNull");
		ds.setUsername("root");
	    ds.setPassword("psa@#2015");
	    //ds.setPassword("root");
	    
		return ds;
		
	}
	
/*	@Bean(name = "oracleSession")
	public LocalSessionFactoryBean sessionFactoryBeanForOracle() {
		LocalSessionFactoryBean sessionFactoryBean = new LocalSessionFactoryBean();
		sessionFactoryBean.setDataSource(oracleDataSource());
		sessionFactoryBean.setPackagesToScan(new String[] {"com.mobios.bank"});
		sessionFactoryBean.setHibernateProperties(hibernatePropertiesForOracle());
		return sessionFactoryBean;
		
	} 
	
	@Bean(name = "oracleDb")
	public DataSource oracleDataSource() {
		DriverManagerDataSource ds = new DriverManagerDataSource();
		ds.setDriverClassName("oracle.jdbc.driver.OracleDriver");
		ds.setUrl("jdbc:mysql://localhost:3306/testdb2?zeroDateTimeBehavior=convertToNull");
		ds.setUsername("root");
		ds.setPassword("root");
		
		return ds;
		
	}
	private Properties hibernatePropertiesForOracle() {
		Properties properties =new Properties();
		properties.put("hibernate.dialect", "org.hibernate.dialect.Oracle10gDialect");
		properties.put("hibernate.show_sql", "true");
		properties.put("hibernate.format_sql", "false");
		return properties;
		
	}*/
	
	private Properties hibernateProperties() {
		Properties properties =new Properties();
		properties.put("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
		properties.put("hibernate.show_sql", "true");
		properties.put("hibernate.format_sql", "false");
		
		return properties;
		
	}
	
	
	
	@Bean
	@Autowired
	public HibernateTransactionManager transactionManager(SessionFactory s) {
		HibernateTransactionManager txManager = new HibernateTransactionManager();
		txManager.setSessionFactory(s);
		
		return txManager;
		
	}
	

}
