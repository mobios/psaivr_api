package com.mobios.bank.config;

import java.sql.Connection;
import java.sql.DriverManager;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import org.springframework.context.annotation.ImportResource;
@ImportResource({"classpath*:context.xml"}) 
public class DatabaseConfig {
	private static DatabaseConfig instance;
	
	private DatabaseConfig() {
	}

	public static DatabaseConfig getInstance() {
		if (instance == null) {
			synchronized (DatabaseConfig.class) {
				if (instance == null) {
					instance = new DatabaseConfig();
				}
			}
		}
		return instance;
	}

	public Connection getMysqlConnection() {

		Connection conn = null;

		try {

			Context initContext = new InitialContext();
			Context envContext = (Context) initContext.lookup("java:/comp/env");
			DataSource ds = (DataSource) envContext
					.lookup("jdbc/police_bank_alert");

			conn = ds.getConnection();

		} catch (Exception e) {

			e.printStackTrace();
		}

		return conn;
	}

	public Connection getOracleConnection() {

		Connection conn = null;

		try {

//			Context initContext = new InitialContext();
//			Context envContext = (Context) initContext.lookup("java:/comp/env");
//			Object a = envContext
//					.lookup("jdbc/police_oracle");
//			DataSource ds = (DataSource) envContext
//					.lookup("jdbc/police_oracle");
//			conn = ds.getConnection();
			
			Class.forName("oracle.jdbc.OracleDriver");  
			  
			//step2 create  the connection object  
			 conn=DriverManager.getConnection("jdbc:oracle:thin:@192.168.128.140:1521:PSA","PSA","MIRINDA");  
			 System.out.println(conn);
		} catch (Exception e) {

			e.printStackTrace();
		}

		return conn;
	}
}
