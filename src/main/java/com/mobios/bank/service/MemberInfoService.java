package com.mobios.bank.service;


import com.mobios.bank.account.Account;
import com.mobios.bank.model.Member_info;
import com.mobios.bank.validation.AccountNotFoundException;
import com.mobios.bank.validation.PINNotValidException;

public interface MemberInfoService {
	
	public Member_info getMemberID(String pin, String msisdn) throws PINNotValidException;
	
	public Account getAccountDetails(String pin, String msisdn, String memberId,int accountType) throws AccountNotFoundException;

}
