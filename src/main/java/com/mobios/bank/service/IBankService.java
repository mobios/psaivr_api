package com.mobios.bank.service;

import com.mobios.bank.account.Account;
import com.mobios.bank.validation.AccountNotFoundException;

public interface IBankService {
	
	public Account getAccount(int accountType, String memberNumber)throws AccountNotFoundException ;
}
