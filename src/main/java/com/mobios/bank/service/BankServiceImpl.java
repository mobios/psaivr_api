package com.mobios.bank.service;


import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;

import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.mobios.bank.account.Account;
import com.mobios.bank.config.DatabaseConfig;
import com.mobios.bank.service.IBankService;
import com.mobios.bank.validation.AccountNotFoundException;


@Service
public class BankServiceImpl implements IBankService {

	public Account getAccount(int accountType, String memberNumber) throws AccountNotFoundException {
		
		Account ac = new Account();
		Connection connection = null;

		CallableStatement statement = null;
		Map<Integer, String> getSubsPeriodDesc = new HashMap<Integer, String>();
		getSubsPeriodDesc.put(1, "per day");
		getSubsPeriodDesc.put(2, "per week");
		getSubsPeriodDesc.put(3, "per month");
		String refNo ="";
		try {
			DatabaseConfig ds= DatabaseConfig.getInstance();
			connection = ds.getOracleConnection();
			if (connection != null) {
				System.out.println("*****************Oracle database Connection..");
//				LogUtil.getLog("Connectionlog").debug("openOracleDBConnection,getAccountbalance,memberNo:"+memberNumber+",accounttype:"+accountType+",refNo:"+ac.getReferenceNo()+",dateTime:"+new Date());
			}
			if (accountType == 10) {
				statement = connection
						.prepareCall("{call GET_CON_BAL(?,?,?,?,?,?,?)} ");// pLAST_PAYMENT_DATE
				statement.setString(1, ac.getReferenceNo());
				statement.setString(2, memberNumber);
				statement.registerOutParameter(3, Types.VARCHAR);
				statement.registerOutParameter(4, Types.FLOAT);
				statement.registerOutParameter(5, Types.FLOAT);
				statement.registerOutParameter(6, Types.FLOAT);
				statement.registerOutParameter(7, Types.FLOAT);
				statement.execute();

				refNo = statement.getString(3);
				float payAmount = statement.getFloat(4);
				float bonus = statement.getFloat(5);
				float sp = statement.getFloat(6);
				float interest = statement.getFloat(7);

				System.out.println("interest*****.." + interest);
				System.out.println("interest*****.." + bonus);
				System.out.println("interest*****.." + sp);
				System.out.println("interest*****.." + payAmount);

				ac.setPayableAmount(new BigDecimal(payAmount));
				ac.setBonus(new BigDecimal(bonus));
				ac.setInstallmentAmount(new BigDecimal(sp));
				ac.setIntallmentInterest(new BigDecimal(interest));

			} else if (accountType == 20) {
				statement = connection
						.prepareCall("{call GET_CL_BAL(?,?,?,?,?,?,?,?)} ");// pLAST_PAYMENT_DATE
				statement.setString(1, ac.getReferenceNo());
				statement.setString(2, memberNumber);
				statement.setInt(3, accountType);
				statement.registerOutParameter(4, Types.VARCHAR);
				statement.registerOutParameter(5, Types.FLOAT);
				statement.registerOutParameter(6, Types.FLOAT);
				statement.registerOutParameter(7, Types.FLOAT);
				statement.registerOutParameter(8, Types.FLOAT);
				statement.execute();
				refNo = statement.getString(4);
				float payAmount = statement.getFloat(5);
				float insAmount = statement.getFloat(6);
				float insInterest = statement.getFloat(7);
				float additinalLoanAmount = statement.getFloat(8);

				ac.setPayableAmount(new BigDecimal(payAmount));
				ac.setInstallmentAmount(new BigDecimal(insAmount));
				ac.setIntallmentInterest(new BigDecimal(insInterest));
				ac.setAdditionalAmount(new BigDecimal(additinalLoanAmount));
			} else if((accountType == 30) || (accountType == 40)){
				statement = connection
						.prepareCall("{call GET_SMCL_BAL(?,?,?,?,?,?,?)} ");// pLAST_PAYMENT_DATE
				statement.setString(1, ac.getReferenceNo());
				statement.setString(2, memberNumber);
				statement.setInt(3, accountType);
				statement.registerOutParameter(4, Types.VARCHAR);
				statement.registerOutParameter(5, Types.FLOAT);
				statement.registerOutParameter(6, Types.FLOAT);
				statement.registerOutParameter(7, Types.FLOAT);
				// statement.registerOutParameter(7, Types.DECIMAL);

				statement.execute();

				refNo = statement.getString(4);
				float payAmount = statement.getFloat(5);
				float insAmount = statement.getFloat(6);
				float insInterest = statement.getFloat(7);

				ac.setInstallmentAmount(new BigDecimal(insAmount));
				ac.setIntallmentInterest(new BigDecimal(insInterest));
				ac.setPayableAmount(new BigDecimal(payAmount));

			}
			else {
				throw new AccountNotFoundException();
			}	

			ac.setAccountType(accountType);

		} catch (Exception e) {
			e.printStackTrace();

		} finally {

			if (statement != null) {
				try {
					statement.close();
					connection.close();
					System.out.println("********connection.close()");
//					LogUtil.getLog("Connectionlog").debug("closeOracleDBConnection,getAccountbalance,memberNo:"+memberNumber+",accounttype:"+accountType+",refNo:"+refNo+",dateTime:"+new Date());
										
				} catch (SQLException sqlEx) {
					sqlEx.printStackTrace();
					statement = null;
				}
			}
		}

		return ac;

	}

	

}
