package com.mobios.bank.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mobios.bank.account.Account;
import com.mobios.bank.dao.MemberInfoDao;
import com.mobios.bank.model.Member_info;
import com.mobios.bank.validation.AccountNotFoundException;
import com.mobios.bank.validation.PINNotValidException;


@Service
@Transactional
public class MemberInfoServiceImpl implements MemberInfoService {
	
	MemberInfoDao memberinfodao;
	
	
	@Autowired
	IBankService ibankService;

	@Autowired
	public void setMemberinfodao(MemberInfoDao memberinfodao) {
		this.memberinfodao = memberinfodao;
	}

	public Member_info getMemberID(String pin, String msisdn) throws PINNotValidException {
		
		return memberinfodao.getMemberID(pin, msisdn);
	}

	public Account getAccountDetails(String pin, String msisdn, String memberId,int accountType)throws AccountNotFoundException {
		
			Account ac = ibankService.getAccount(accountType, memberId);
		
		return ac;
	}
	
	

}
