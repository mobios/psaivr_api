package com.mobios.bank.validation;

public class AccountNotFoundException extends PSAException{
	
	private static final long serialVersionUID = 1L;
	private static String errorMessage = "Account type mismatch.Account type is not in the range of 10-40";
	private static int errorCode = PSAExceptionCodes.ACCOUNT_TYPE_NOT_FOUND;

	public AccountNotFoundException() {
		super(errorMessage, errorCode);
	 }

}
