package com.mobios.bank.validation;

public class PSAException extends Exception {
	private static final long serialVersionUID= 100L;
	
	private String errorMessage;
	private int errorCode;

	public String getErrorMessage() {
		return errorMessage;
	}
	public int getErrorCode(){
		return errorCode;
	}
	public PSAException(String errorMessage) {
		super(errorMessage);
		this.errorMessage = errorMessage;
	}
	public PSAException(String errorMessage, int errorCode) {
		super(errorMessage);
		this.errorMessage = errorMessage;
		this.errorCode=errorCode;
	}
	public PSAException() {
		super();
	}
	

}
