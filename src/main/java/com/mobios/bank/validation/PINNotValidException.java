package com.mobios.bank.validation;

public class PINNotValidException extends PSAException{
		
	private static final long serialVersionUID = 1L;
	private static String errorMessage = "Pin Number and Mobile Number mismatch.";
	private static int errorCode = PSAExceptionCodes.PIN_VALIDATION_FAIL;

	public PINNotValidException() {
		super(errorMessage, errorCode);
	 }
	    
 
}

