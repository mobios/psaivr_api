package com.mobios.bank.validation;

public class PSAExceptionCodes {
	
	public static final int PIN_VALIDATION_FAIL = 251;      //Pin and mobile mismatch 
	public static final int ACCOUNT_TYPE_NOT_FOUND = 300;     //Account type mismatch
	public static final int NO_MEMBER_FOUND = 250;		      //Member Not Found
	public static final int MEMBER_NOT_REGISTERED = 256;      //Member not registered
		

}
